# RestiCron - auto Linux server backup to BackBlaze (or S3)
- Auto backup your server to BackBlaze or any other S3 storage
- Based on Restic [https://restic.net/]

## Features
- run daily backup from cron
- auto delete old backup with configurable retention
- mail alert when some error occrured
- optionally send check ping to https://healthchecks.io/

## Usage
- install Restic `sudo apt install restic` or `yum install restic`
- copy template config `cp resticron.conf.template resticron.conf`
- edit resticron.conf
- init repository `./resticron.sh init`
- install to crontab `./resticron cron-install`
- commandline paramaters
```
[sudo] resticron.sh cron - run silent backup from cron and send fail mail
[sudo] resticron.sh cron-install - install daily crontab
[sudo] resticron.sh backup - run normal backup from cli with predefined config
[sudo] resticron.sh [command] - passtrough to original Restic command
```
- show backups (snapshots)
```
./resticron snapshots
```