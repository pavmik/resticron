#!/usr/bin/env bash

# Restic backup wrapper for cron
# pavel@mikulka.info

############################################################


cd "$(dirname "$0")"
KEEP_LAST=7
KEEP_DAILY=0
KEEP_WEEKS=4
source ./resticron.conf
export B2_ACCOUNT_ID
export B2_ACCOUNT_KEY
export RESTIC_REPOSITORY
export RESTIC_PASSWORD
ME=`basename "$0"`
LOG="resticron.log"


function usage { 
    echo
    echo "========================================================================"
    echo "Wrapper usage:"
    echo "   "[sudo] $ME cron" - run silent backup from cron and send fail mail"
    echo "   "[sudo] $ME cron-install" - install daily crontab"
    echo "   "[sudo] $ME backup" - run normal backup from cli with predefined config"
    echo "   "[sudo] $ME [command]" - passtrough to original Restic command"
    echo "========================================================================"
    echo
    }


case $1 in
  cron)
    echo "#################" >> $LOG
    date >> $LOG
    restic backup $DIRS &>> $LOG
    EXIT_STATUS=$?
    if [ $EXIT_STATUS -ne 0 ] ;then
      tail -n 100 $LOG | mail -s "`hostname` restic backup FAIL" $MAIL
      #echo "Backup failed"
    elif [ $EXIT_STATUS -eq 0 ] ;then
      restic forget -q --keep-weekly=$KEEP_WEEKS --keep-daily=$KEEP_DAILY --keep-last=$KEEP_LAST
      curl -s -m 10 --retry 5 https://hc-ping.com/$HCTOKEN > /dev/null
      #echo "Backup finished"
    fi;
    ;;
  backup)
    restic backup $DIRS
    ;;
  cron-install)
    crontab -l | { cat; echo "0 1 * * * sudo $DIR/$ME cron"; } | crontab -
    ;;
  *)
    usage
    restic $@
    usage
    ;;
esac